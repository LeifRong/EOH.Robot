﻿using EOH.Robot.MessageAgrs;

using NewLife.Log;
using NewLife.Remoting;

using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace EOH.Robot
{
    public interface IAgrs { }
    /// <summary>Robot服务类</summary>
    public class RobotService
    {
        /// <summary>性能追踪</summary>
        protected ITracer? Tracer { get; set; } = DefaultTracer.Instance;
        HttpClient? _Client;
        protected async Task<Object?> PostAsync(string webhook, Object agrs)
        {
            _Client ??= Tracer.CreateHttpClient();
            var res = await _Client.PostAsync<Object>(webhook, agrs);
            return res;
        }

        /// <summary>发送消息</summary>
        public virtual void Send(string webhook, IAgrs? agrs)
        {
            if (agrs is null || string.IsNullOrWhiteSpace(webhook)) { return; }
            if ((webhook.Contains("qyapi.weixin") && !(agrs is WeiXinMessageAgrs)) ||
               (webhook.Contains("oapi.dingtalk") && !(agrs is DingTalkMessageAgrs)) ||
               (webhook.Contains("open.welink") && !(agrs is WeLinkMessageAgrs))) { throw new Exception($"agrs is not {webhook}"); }
            PostAsync(webhook, agrs).ConfigureAwait(false);
        }
        public virtual void Send(MessageSendType sendType, string token, IAgrs? agrs)
        {
            if (agrs is null || string.IsNullOrWhiteSpace(token)) { return; }
            Send(sendType switch
            {
                MessageSendType.WeLink => $"https://open.welink.huaweicloud.com/api/werobot/v1/webhook/send?token={token}&channel=standard",
                MessageSendType.DingTalk => $"https://oapi.dingtalk.com/robot/send?access_token={token}",
                MessageSendType.QyWeiXin => $"https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={token}",
                MessageSendType.FeiShu => $"https://open.feishu.cn/open-apis/bot/v2/hook/{token}",
                _ => throw new Exception("未知的消息类型")
            }, agrs);
        }
    }

    /// <summary>消息类型</summary>
    public enum MessageSendType
    {
        /// <summary>企业微信</summary>
        QyWeiXin,
        /// <summary>钉钉</summary>
        DingTalk,
        /// <summary>WeLink</summary>
        WeLink,
        /// <summary>飞书</summary>
        FeiShu
    }
}
